JSONCookie
==========

A JavaScript function that wraps the jQuery cookie plugin (http://plugins.jquery.com/project/Cookie) and saves all data in JSON format.

Currently only saves to the root of the domain and has an expiration of nothing i.e. session based.

Very simple interface of get(), set() and delete(), handling full cookie contents or single keys.

Fully tested. Uses QUnit, $.cookie has been stubbed.

Requirement jQuery and jQuery cookie plugin.

Example of usage is $.JSONCookie('cookie-name').({ key; 'value' }). More can be seen in json-cookie.test.js


TODO
----

1. Enable option settings for path, expiration, etc.