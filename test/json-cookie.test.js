var cookie,
		name = 'my-name',
		value = 'my-value',
		key = 'my-key';

module('json-cookie', {
	setup: function () {
		cookie = $.JSONCookie(name);
		
		var cookieName, data;
		
		$.cookie = function () {
			if (arguments.length === 1 && arguments[0] === cookieName) {
				return data;
			} else if (arguments.length === 3) {
				cookieName = arguments[0];
				data = arguments[1];
			}
		}
	},
	teardown: function () {
		delete cookie;
	}
});

function createCookie () {
	cookie.set({ key: value });
};

test('Can create a cookie.', function () {
	cookie.set();
	
	deepEqual(JSON.parse($.cookie(name)), {}, 'Cookie has not been created.');
});

test('Can retrieve a cookie.', function () {
	cookie.set();
	var result = cookie.get();
	
	deepEqual(result, {}, 'Cookie has not been retrieved.');
});

test('Can get the name of the cookie.', function () {
	var result = cookie.getName();

	equals(result, name, 'Cookie has not returned the correct name.');
});

test('Returns null for a nonexistent cookie', function () {
	var result = cookie.get('missing');
	
	equals(result, null, 'Nonexistent cookie does not return null.');
});

test('Can set all data for a cookie.', function () {
	createCookie();
	
	equals(cookie.get().key, value, 'Cookie data has not been set.');
});

test('Can delete all data for a cookie.', function () {
	createCookie();
	cookie.deleteData();
	
	deepEqual(cookie.get(), {}, 'Cookie has not been deleted.');
});

test('Can retrieve a single key for a cookie.', function () {
	createCookie();
	
	var result = cookie.get(key);
	
	equals(result, value, 'Single key has not been retrieved.');
});

test('Can add an additional key to a cookie while maintaining existing data.', function () {
	createCookie();
	var value2 = 'your-value';
	var data = { key2: value2 };
	cookie.set(data);
	
	var result = cookie.get();
	
	equals(result.key, value, 'Original data has not been maintained.');
	equals(result.key2, value2, 'New value is not correct.');
});

test('Can delete a key for a cookie while maintaining other data.', function () {
	createCookie();
	var value2 = 'anotherValue';
	var key2 = 'key2';
	var data = { key2: value2 };
	cookie.set(data);
	cookie.deleteData(key2);
	
	var result = cookie.get();
	
	var props, count = 0;
	for (props in result) {
		if (result.hasOwnProperty(props)) {
			count++;
		}
	}
	equals(count, 1, 'Key has not been removed from object.');
	equals(result.key, value, 'Original data has not been maintained.');
	equals(result.key2, undefined, 'Data to be deleted still exists.');
});

test('Can have multiple cookies', function () {
	createCookie();
	var nameResult1 = cookie.getName();
	var valueResult1 = cookie.get(key);

	var key2 = 'key2',
		value2 = 'value2';
	var newCookie = $.JSONCookie('new-cookie');
	newCookie.set({ key2: value2 });
	var newCookieName = newCookie.getName();
	var valueResult2 = newCookie.get().key2;

	equals(nameResult1, name, 'Cookie has not returned the correct name.');
	equals(valueResult1, value, 'Cookie1 has not returned the correct value.');
	equals(valueResult2, value2, 'Cookie2 has not returned the correct value.');
	equals(newCookieName, 'new-cookie', 'Cookie has not returned the correct name.');
});