(function ($) {
	$.JSONCookie = function JSONCookie (cookieName) {

	var name = cookieName;

	function getName () {
		return name;
	};

	function set (data) {
		var cookie = get();
		if (cookie) {
			$.extend(data, cookie);
		}
		$.cookie(name, JSON.stringify( !data ? {} : data ), { path: '/' });
	};

	function get (key) {
		var cookie = $.cookie(name);
		if (cookie) {
			var data = JSON.parse(cookie);
			if (key) {
				return data.key;
			}
			return data;
		}
		return null;
	};

	function deleteData (key) {
		var data = {};
		if (key) {
			data = get();
			delete data[key];
		}
		$.cookie(name, JSON.stringify(data), { path: '/' });	
	};

	return {
		set: set,
		get: get,
		deleteData: deleteData,
		getName: getName
	}
}
})(jQuery);